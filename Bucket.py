import json
import threading
import unicodedata
import time
import os
import os.path
import random
import string

from flask import Flask, request, redirect, url_for, render_template, session, flash, jsonify
from flask.ext.redis import FlaskRedis
import pymysql.cursors
from werkzeug import secure_filename
import flickr_api
import requests
import tweepy
import xmltodict
import urllib2
import urllib
from lxml import html
from BeautifulSoup import BeautifulSoup

#twitter setup starts
consumer_key = 'gdX4Zb3uu52N8TPpHmkyCbVDE'
consumer_secret = 'j2n666bbbUOROn4n6EUsMtmZGHKE2wnkeO0FwuHeN4B6xhb3Bk'
access_token = '2592993680-OMlGCClzkZADucBERw5XNEe9HplrcZpRYzcv1xa'
access_token_secret = 'NbMLB9SfNsjqeGziZuOe1zHWWDKkzr8sfrVBzwlAJ7lKk'
# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)
#twitter setup ends

#flickr setup starts
flickr_api.set_keys(api_key = 'e52742e2b7db755cef9617c2cc6fe643', api_secret = '22181edfee523576')
#flickr setup ends

#mysql connection starts server_password = "#admin1@"
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='admin1@',
                             db='bucket',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
#mysql connection ends

UPLOAD_FOLDER = 'static/images'
ALLOWED_EXTENSIONS = set(['jpg', 'jpeg'])
                
app = Flask(__name__)
app.secret_key = os.urandom(24)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#redis setup starts
#REDIS_URL = "redis://password:@localhost:6379/0"
REDIS_URL = "redis://:@127.0.0.1:6379/0"
redis_store = FlaskRedis(app)
#redis setup ends

@app.route("/")
def login_form():
    if '_username' in session:
        return redirect(url_for('dashboard'))
    else:
        return render_template('index.html')
    

@app.route("/login", methods=['POST'])
def login():
    if request.method == 'POST':
        username = request.form['_username']
        password = request.form['_password']

        if username != None and username != '' and password != None and password != '':
            
            with connection.cursor() as cursor:
                sql = "SELECT `admin_uname`,`admin_id` FROM `admin_login` WHERE `admin_uname`='"+ username +"' AND `admin_password`='"+ password +"'"
                cursor.execute(sql)
                result = cursor.fetchone()

                if result:
                    session['_username'] = result['admin_uname']
                    session['_user_id']  = result['admin_id']
                    response = '1'
                else:
                    response = '3'

        else:
            response = '2'

    else:
        response = '0'

    return response


@app.route("/dashboard")
def dashboard():
    if '_username' in session:
        with connection.cursor() as cursor:
            #twitter trends
            sql_all_trends_twitter = "SELECT * FROM `all_trends` WHERE `all_trends_site`='twitter' ORDER BY `all_trends_id` DESC LIMIT 1" 
            cursor.execute(sql_all_trends_twitter)
            all_trends_twitter = cursor.fetchone()

            if all_trends_twitter:
                sql_trends_twitter = "SELECT * FROM `trends` WHERE `all_trends_id`='"+str(all_trends_twitter['all_trends_id'])+"'" 
                cursor.execute(sql_trends_twitter)
                trends_twitter = cursor.fetchall()
            else:
                trends_twitter = None

            #google trends
            sql_all_trends_google = "SELECT * FROM `all_trends` WHERE `all_trends_site`='google' ORDER BY `all_trends_id` DESC LIMIT 1" 
            cursor.execute(sql_all_trends_google)
            all_trends_google = cursor.fetchone()

            if all_trends_google:
                sql_trends_google = "SELECT * FROM `trends` WHERE `all_trends_id`='"+str(all_trends_google['all_trends_id'])+"'" 
                cursor.execute(sql_trends_google)
                trends_google = cursor.fetchall()
            else:
                trends_google = None

        return render_template('dashboard.html',ttrends=trends_twitter,gtrends=trends_google)
    else:
        flash("Please Login to Open Dashboard!")
        return redirect(url_for('login_form'))

@app.route("/logout")
def logout():
    if '_username' in session and '_user_id' in session:
        session.pop('_username',None)
        session.pop('_user_id', None)
    
    return(redirect(url_for('login_form')))

@app.route('/add_bucket')
def add_bucket():
    if '_username' in session:
        return render_template('add_bucket.html')
    else:
        return(redirect(url_for('login_form')))

@app.route('/bucket_name', methods=['POST','GET'])
def bucket_name():
    if request.method == 'POST':
        uid = str(int(request.form['_user_id']))
        unm = request.form['_username']
        
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `admin_login` WHERE `admin_id`='"+ uid +"' AND `admin_uname`='"+ unm +"'" 
            cursor.execute(sql)
            is_login = cursor.rowcount

        if is_login > 0:

            #name = request.args.get('name')
            name = request.form['name']
            
            with connection.cursor() as cursor:
                sql = "SELECT `name`,`id` FROM `bucket` WHERE `name`='"+ name +"'" 
                cursor.execute(sql)
                result = cursor.fetchone()

                if not result :
                    datetime = time.strftime("%Y-%m-%d %H:%M:%S")
                    sql = "INSERT INTO `bucket` (`name`,`datetime`) VALUES ('"+ name +"','"+ datetime +"')"
                    cursor.execute(sql)
                    connection.commit()

                    #sql = "SELECT `id` FROM `bucket` WHERE `name`='"+ name +"'" 
                    #cursor.execute(sql)
                    #result = cursor.fetchone()
                    #b_id = result['id']
                    b_id = cursor.lastrowid

                    #starts redis store data
                    redis_store.hmset('bucket:'+str(b_id),{'id':str(b_id),'name':name,'datetime':datetime})
                    redis_store.sadd('bucket:all',str(b_id))
                    #ends redis store data
                    
                    ret_val = '1&Bucket Successfully Added!'

                    session['add_bucket_id'] = b_id
                else:
                    ret_val = '0&Bucket Name Allready Exist!'
        else:
            ret_val = "2&Please Login to Add Bucket!"

    else:
        ret_val = "2&Wrong Request!"

    return ret_val


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/add_image', methods=['POST'])
def add_image():
    if request.method == 'POST':
        uid = str(int(request.form['_user_id']))
        unm = request.form['_username']
        
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `admin_login` WHERE `admin_id`='"+ uid +"' AND `admin_uname`='"+ unm +"'" 
            cursor.execute(sql)
            is_login = cursor.rowcount

        if is_login > 0:

            bid = str(int(request.form['id']))
            
            search_imgs = request.form['select_imgs']
            #search_imgs = search_imgs.split(',')
            
            #return "0&"+search_imgs

            if search_imgs == '' or search_imgs == None or search_imgs == 'undefined':
                file1 = request.files['image']

                if not file1:
                    response = "0&Please Select Image."
                    #flash(response,"add_bucket_msg")
                    #return(redirect(url_for('add_bucket')))
                    return response

                elif allowed_file(file1.filename):
                    filename = secure_filename(file1.filename)
                    filenametoupload = str(int(time.time()))+filename
                    
                    if os.path.isfile(UPLOAD_FOLDER+"/"+filenametoupload):
                        response = "0&Image Name Allready Exist! Please Rename Image."
                        #flash(response,"add_bucket_msg")
                        #return(redirect(url_for('add_bucket')))
                        return response
                                
                else :
                    response = "0&Only '.jpg' and '.jpeg' Format is Allowed."
                    #flash(response,"add_bucket_msg")
                    #return(redirect(url_for('add_bucket')))
                    return response



            with connection.cursor() as cursor:
                sql = "SELECT `name` FROM `bucket` WHERE `id`='"+ bid +"'" 
                cursor.execute(sql)
                result = cursor.fetchone()

                if not result:
                    response = "1&There is No Record Found."
                else:
                    if not os.path.exists(UPLOAD_FOLDER):
                        os.makedirs(UPLOAD_FOLDER)

                    if search_imgs != '' and search_imgs != 'undefined':
                        filename = 'flickr'
                        filename += ''.join(random.choice(string.ascii_lowercase) for j in range(12))
                        filename += '.jpg'
                        filenametoupload = str(int(time.time()))+filename

                        if not os.path.isfile(UPLOAD_FOLDER+"/"+filenametoupload):
                            urllib.urlretrieve(search_imgs, os.path.join(app.config['UPLOAD_FOLDER'], filenametoupload))



                    if search_imgs == '' or search_imgs == 'undefined':
                        file1 = request.files['image']
                        if file1 and allowed_file(file1.filename):
                            filename = secure_filename(file1.filename)
                            filenametoupload = str(int(time.time()))+filename
                            
                            if not os.path.isfile(UPLOAD_FOLDER+"/"+filenametoupload):
                                file1.save(os.path.join(app.config['UPLOAD_FOLDER'], filenametoupload))

                            
                    files    = filenametoupload
                    datetime = time.strftime("%Y-%m-%d %H:%M:%S")
                    weight   = request.form['weight']
                    header   = request.form['header']
                    body     = request.form['body']
                    sql = "INSERT INTO `images` (`filename`,`bucket_id`,`datetime`,`weight`,`header`,`body`) VALUES ('"+ files +"','"+ bid +"','"+ datetime +"','"+ weight +"','"+ header +"','"+ body +"')"
                    cursor.execute(sql)
                    connection.commit()

                    #starts redis store data
                    #sqlfetchback = "SELECT `id` FROM `images` WHERE `filename`='"+files+"' AND `datetime`='"+datetime+"' AND `bucket_id`='"+bid+"'"
                    #cursor.execute(sqlfetchback)
                    #imgfetchback = cursor.fetchone()
                    #iid = imgfetchback['id']
                    iid = cursor.lastrowid

                    redis_store.hmset('bucket:'+str(bid)+':image:'+str(iid),{'filename':files,'bucket_id':str(bid),'datetime':datetime,'weight':weight,'header':header,'body':body })
                    redis_store.sadd('bucket:'+str(bid)+':image:all',str(iid))
                    #ends redis store data

                    response = "1&Image Successfully Uploaded."
                    #session.pop('add_bucket_id',None)

        else:
            response = "2&Please Login to Add Image."

    else:
        response = "2&Wrong Request."

    #flash(response,"img_upld_suc")

    #return(redirect(url_for('bucket_list')))
    return response


@app.route('/bucket_list', methods=['POST','GET'])
def bucket_list():
    if '_username' in session:
        if request.method == 'POST':
            bid = str(int(request.form['bid']))

            with connection.cursor() as cursor:
                sql = "SELECT `name`,`id` FROM `bucket`" 
                cursor.execute(sql)
                bucket = cursor.fetchall()

                if bid == '0':
                    if not bucket:
                        return(render_template('bucket_list.html',response="There is No Record Found."))
                    else:
                        return(render_template('bucket_list.html',buckets=bucket))
                        
                else:
                    sql = "SELECT * FROM `images` WHERE `bucket_id`='"+ bid +"'" 
                    cursor.execute(sql)
                    bucket_images_co = cursor.rowcount

                    allimgsredisid = list(redis_store.smembers('bucket:'+str(bid)+':image:all'))
                    countimgs = len(allimgsredisid)
                    allimgsredis = []
                    for z in range(0,countimgs):
                        allimgsredis.append(redis_store.hgetall('bucket:'+str(bid)+':image:'+str(allimgsredisid[z])))


                    if bucket_images_co > 0:
                        bucket_images = cursor.fetchall()
                        return(render_template('bucket_list.html',buckets=bucket,bucket_images=bucket_images,bid=bid,allimgsredis=allimgsredis))
                    else:
                        return(render_template('bucket_list.html',buckets=bucket,errornoimgs="No Images Uploaded",bid=bid))

        else:
            with connection.cursor() as cursor:
                sql = "SELECT `name`,`id` FROM `bucket`" 
                cursor.execute(sql)
                bucket = cursor.fetchall()

                if not bucket:
                    return(render_template('bucket_list.html',response="There is No Bucket Found."))
                else:
                    return(render_template('bucket_list.html',buckets=bucket))
                    
    else:
        flash("Please Login to See Bucket List!")
        return redirect(url_for('login_form'))

@app.route('/bucket_update_form', methods=['GET'])
def bucket_update_form():
    if '_username' in session:
        if request.method == 'GET':
            bid = str(int(request.args['bid']))

            with connection.cursor() as cursor:
                sql = "SELECT `name`,`id` FROM `bucket` WHERE `id`='"+ bid +"'" 
                cursor.execute(sql)
                bucket = cursor.fetchall()

            if bucket:
                with connection.cursor() as cursor:
                    sqlimg = "SELECT `filename` FROM `images` WHERE `bucket_id`='"+ bid +"'" 
                    cursor.execute(sqlimg)
                    images = cursor.fetchone()

                    if images:
                        images = images['filename'].split(',')
                        return(render_template('update_bucket.html',buckets=bucket,images=images))
                    else:
                        return(render_template('update_bucket.html',buckets=bucket,errornoimg="No Images Found."))

            else:
                return(render_template('update_bucket.html',errornorec="Bucket Not Exist."))

        else:
            flash("Wrong Request.")
            return(redirect(url_for('login_form')))
    else:
        flash("Please Login to Update Bucket!")
        return redirect(url_for('login_form'))


@app.route('/update_bucket', methods=['POST'])
def update_bucket():
    if request.method == 'POST':
        uid = str(int(request.form['_user_id']))
        unm = request.form['_username']
        
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `admin_login` WHERE `admin_id`='"+ uid +"' AND `admin_uname`='"+ unm +"'" 
            cursor.execute(sql)
            is_login = cursor.rowcount

        if is_login > 0:

            bucket_id = str(int(request.form['bucket_id']))
            counter = 0
            with connection.cursor() as cursor:
                sqlcounter = "SELECT * FROM `images` WHERE `bucket_id`='"+ bucket_id +"'" 
                cursor.execute(sqlcounter)
                counter = cursor.rowcount
                
            response = ''
            filenametoupload = ''
            filenametodelete = ''

            for k in range(1,int(counter)+1):
                iid = str(int(request.form['id'+str(k)]))
                file1 = request.files['image'+str(k)]

                if file1 and allowed_file(file1.filename):
                    filename = secure_filename(file1.filename)
                    filenametoupload = str(int(time.time()))+filename
                    
                    if os.path.isfile(UPLOAD_FOLDER+"/"+filenametoupload):
                        response = "0&Image Name Allready Exist! Please Rename Image."
                        session["error_update_bucket"] = response
                        #return(redirect(url_for('bucket_update_form',bid=bid)))
                        return response
                                
                elif file1 :
                    response = "0&Only '.jpg' and '.jpeg' Format is Allowed."
                    session["error_update_bucket"] = response
                    #return(redirect(url_for('bucket_update_form',bid=bid)))
                    return response

                with connection.cursor() as cursor:
                    sqloldimg = "SELECT * FROM `images` WHERE `id`='"+ iid +"'" 
                    cursor.execute(sqloldimg)
                    oldimgco = cursor.rowcount

                    if oldimgco > 0:
                        oldimg = cursor.fetchone()

                        if oldimg :
                            images = oldimg['filename']

                            if not os.path.exists(UPLOAD_FOLDER):
                                os.makedirs(UPLOAD_FOLDER)

                            file1 = request.files['image'+str(k)]
                            if file1 and allowed_file(file1.filename):
                                filename = secure_filename(file1.filename)
                                filenametodelete = images
                                images = str(int(time.time()))+filename
                                
                                if not os.path.isfile(UPLOAD_FOLDER+"/"+images):
                                    file1.save(os.path.join(app.config['UPLOAD_FOLDER'], images))

                            if filenametodelete != '':
                                if os.path.isfile(UPLOAD_FOLDER+"/"+filenametodelete):
                                    os.remove(UPLOAD_FOLDER+"/"+filenametodelete)

                            files = images

                    else:
                        if not os.path.exists(UPLOAD_FOLDER):
                            os.makedirs(UPLOAD_FOLDER)

                        file1 = request.files['image'+str(k)]
                        if file1 and allowed_file(file1.filename):
                            filename = secure_filename(file1.filename)
                            filenametoupload = str(int(time.time()))+filename
                            
                            if not os.path.isfile(UPLOAD_FOLDER+"/"+filenametoupload):
                                file1.save(os.path.join(app.config['UPLOAD_FOLDER'], filenametoupload))

                        files = filenametoupload

                    datetime = time.strftime("%Y-%m-%d %H:%M:%S")
                    header   = request.form['header'+str(k)]
                    weight   = request.form['weight'+str(k)]
                    body     = request.form['body'+str(k)]
                    sql = "UPDATE `images` SET `filename`='"+ files +"',`header`='"+ header +"',`body`='"+ body +"',`weight`='"+ weight +"' WHERE `id`='"+ iid +"'"
                    cursor.execute(sql)
                    connection.commit()

                    #starts redis update
                    redis_store.hmset('bucket:'+str(bucket_id)+':image:'+str(iid),{'filename':files,'header':header,'body':body,'weight':weight})
                    #ends redis update

                    response = "1&Bucket Successfully Updated."
                    session.pop('error_update_bucket',None)
                    
            flash('Bucket Successfully Updated.','update_bucket')
            return response

        else:
            response = "2&Please Login To Update Bucket!"
            return response

    else:
        response = "2&Invalid Request to Update Bucket!"
        flash("2&Invalid Request to Update Bucket!")
        #return redirect(url_for('login_form'))
        return response


@app.route('/delete_bucket', methods=['POST'])
def delete_bucket():
    if request.method == 'POST':
        uid = str(int(request.form['_user_id']))
        unm = request.form['_username']
        
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `admin_login` WHERE `admin_id`='"+ uid +"' AND `admin_uname`='"+ unm +"'" 
            cursor.execute(sql)
            is_login = cursor.rowcount

        if is_login > 0:

            bid = str(int(request.form['bid']))
            
            with connection.cursor() as cursor:
                sql = "SELECT * FROM `bucket` WHERE `id`='"+ bid +"'" 
                cursor.execute(sql)
                bucketco = cursor.rowcount

                if bucketco > 0:
                    bucket = cursor.fetchone()

                    if bucket:
                        sqlimg = "SELECT `filename` FROM `images` WHERE `bucket_id`='"+ bid +"'" 
                        cursor.execute(sqlimg)
                        imgsco = cursor.rowcount

                        if imgsco > 0:
                            imgs = cursor.fetchall()

                            if imgs:
                                for img in imgs :
                                    if img['filename'] != None or img['filename'] != '':
                                        image = img['filename']
                                        if os.path.isfile(UPLOAD_FOLDER+"/"+image):
                                            os.remove(UPLOAD_FOLDER+"/"+image)
                                    
                    sqldel = "DELETE FROM `bucket` WHERE `id`='"+ bid +"'" 
                    cursor.execute(sqldel)
                    connection.commit()

                    #starts remove from redis
                    redis_store.delete('bucket:'+str(bid))
                    redis_store.srem('bucket:all',str(bid))
                    allimgs = redis_store.smembers('bucket:'+str(bid)+':image:all')
                    for img in allimgs:
                        redis_store.delete('bucket:'+str(bid)+':image:'+str(img))
                    redis_store.delete('bucket:'+str(bid)+':image:all')
                    #ends remove from redis

                    response = '1&Bucket Successfully Deleted.'
                    return response
                    
                else:
                    response = '3&No Bucket Exist to delete!'
                    return response
        else:
            response = '2&Please Login to Delete Bucket!'
            return response          

    else:
        response = '0&Invalid Request to Delete Bucket!'
        return response


@app.route('/search_imgs', methods=['POST'])
def search_imgs():
    uid = str(int(request.form['_user_id']))
    unm = request.form['_username']
    
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `admin_login` WHERE `admin_id`='"+ uid +"' AND `admin_uname`='"+ unm +"'" 
        cursor.execute(sql)
        is_login = cursor.rowcount

    if is_login > 0:

        if request.method == 'POST':
            search_val = request.form['search']

            if search_val == '' or search_val ==None:
                return '3&Please Enter Search Value to Search.'

            search = flickr_api.method_call.call_api(
                    method = "flickr.photos.search",
                    api_key='e52742e2b7db755cef9617c2cc6fe643',
                    text=search_val,
                    sort='relevance',
                    per_page='30')
        
            """imgs = '<br style="clear:both"><div class="row"><div class="col-lg-12"><h1 class="page-header">'+ search_val +'</h1>'

            for photo in search['photos']['photo']:
                imgs += '<div class="col-lg-3 col-md-4 col-xs-6 thumb"><a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox"><img src="https://farm1.staticflickr.com/'+photo['server']+'/'+photo['id']+'_'+photo['secret']+'.jpg" style="height:100px" class="img-responsive" /> </a></div>'  

            imgs += '</div></div>'"""

            imgs = '1&<select class="image-picker show-html" name="select_imgs" id="select_imgs" data-limit="1"><option value=""></option>'

            for photo in search['photos']['photo']:
                imgs += '<option data-img-src="https://farm1.staticflickr.com/'+photo['server']+'/'+photo['id']+'_'+photo['secret']+'.jpg" value="https://farm1.staticflickr.com/'+photo['server']+'/'+photo['id']+'_'+photo['secret']+'.jpg">  Page 1  </option>'
            
            imgs += '</select>'

            return imgs

        else:
            response = '0&Invalid Request to Search Images!'
            return response

    else:
        response = '2&Please Login to Search Images!'
        return response


@app.route('/new_bucket_form', methods=['POST'])
def new_bucket_form():
    if 'add_bucket_id' in session :
        session.pop('add_bucket_id',None)

    return '1'


@app.route('/get_session', methods=['POST'])
def get_session():
    if '_username' in session and '_user_id' in session:
        admin = [('_username', session['_username']),('_user_id', session['_user_id'])]

        return jsonify(admin)
    else:
        return '0'


def convert(xml_data, xml_attribs=True):
    d = xmltodict.parse(xml_data, xml_attribs=xml_attribs)
    #return json.dumps(d['feed']['entry']['content']['#text'])
    return d


#@app.route('/trendscron')
def trendscron():
    threading.Timer(3600, trendscron).start()
    #return str(api.rate_limit_status())
    #twitter trends
    ttrends = api.trends_place(1) #twitter
    trends_datetime = time.strftime("%Y-%m-%d %H:%M:%S")

    with connection.cursor() as cursor:
        trends_site = 'twitter'

        sql_all_trends = "INSERT INTO `all_trends` (`all_trends_datetime`,`all_trends_site`) VALUES('"+trends_datetime+"','"+trends_site+"')" 
        cursor.execute(sql_all_trends)
        connection.commit()
        
        sql_get_last_trends = "SELECT * FROM `all_trends` WHERE `all_trends_datetime`='"+trends_datetime+"' AND `all_trends_site`='"+trends_site+"' " 
        cursor.execute(sql_get_last_trends)
        last_trends = cursor.fetchone()

        last_trend = str(last_trends['all_trends_id'])

        for trends in ttrends:
            co = 0
            for trend in trends['trends']:
                if co >= 20:
                    break
                trends_keyword = unicodedata.normalize('NFKD', trend['name']).encode('ascii','ignore')
                
                sql_trends = "INSERT INTO `trends` (`trends_keyword`,`trends_datetime`,`trends_site`,`all_trends_id`) VALUES('"+trends_keyword+"','"+trends_datetime+"','"+trends_site+"','"+last_trend+"')"
                cursor.execute(sql_trends)
                connection.commit()
                co+=1

        
    #google trends
    gtrendspage = requests.get('http://www.google.com/trends/hottrends/atom/hourly') #google
    gt = convert(xml_data=gtrendspage.text,xml_attribs=True)            
    gtrendsxml = gt['feed']['entry']['content']['#text']

    gtrends = []

    soup = BeautifulSoup(gtrendsxml)
    soup.prettify()
    ol = soup.find("ol")
    for li in ol.findAll("li"):
        for span in li.findAll("span"):
            for a in span.findAll("a"):
                gtrends.append(a.text)

    with connection.cursor() as cursor:
        trends_site = 'google'

        sql_all_trends = "INSERT INTO `all_trends` (`all_trends_datetime`,`all_trends_site`) VALUES('"+trends_datetime+"','"+trends_site+"')" 
        cursor.execute(sql_all_trends)
        connection.commit()

        sql_get_last_trends = "SELECT * FROM `all_trends` WHERE `all_trends_datetime`='"+trends_datetime+"' AND `all_trends_site`='"+trends_site+"' " 
        cursor.execute(sql_get_last_trends)
        last_trends = cursor.fetchone()

        last_trend = str(last_trends['all_trends_id'])

        for gtrend in gtrends:
            co = 0
            if co >= 20:
                break
            trends_keyword = unicodedata.normalize('NFKD', gtrend).encode('ascii','ignore')
            
            sql_trends = "INSERT INTO `trends` (`trends_keyword`,`trends_datetime`,`trends_site`,`all_trends_id`) VALUES('"+trends_keyword+"','"+trends_datetime+"','"+trends_site+"','"+last_trend+"')" 
            cursor.execute(sql_trends)
            connection.commit()
            co+=1
        
    return '1'

trendscron()


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80,debug=True)



