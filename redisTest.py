from flask import Flask
import os
from flask.ext.redis import FlaskRedis
import time
from flask import jsonify
import json

app = Flask(__name__)
app.secret_key = os.urandom(24)
REDIS_URL = "redis://:@localhost:6379/0"
redis_store = FlaskRedis(app)

@app.route('/')
def home():
	return 'Home'

@app.route('/redis_test')
def redis_test():
	#redis_store.set('name', 'Vasim Seta')
	#redis_store.delete('name')

	id = '1'
	name = 'vasim seta'
	datetime = time.strftime("%Y-%m-%d %H:%M:%S")

	iid = '11'
	filename = 'myimage.jpg'
	weight   = '2'
	header   = 'testing with redis'
	body     = 'testing testing testing'

	redis_store.hmset('bucket:'+id,{'id':id,'name':name,'datetime':datetime})
	redis_store.sadd('bucket:all','1')
	redis_store.hmset('bucket:'+id+':image:'+iid,{'iid':iid,'filename':filename,'datetime':datetime,'weight':weight,'header':header,'body':body})

	alldata = redis_store.smembers('bucket:all')

	#return ','.join(alldata)
	alldata = list(alldata)
	return alldata[1]

if __name__ == "__main__":
	app.run(host='0.0.0.0',port=9336,debug=True)