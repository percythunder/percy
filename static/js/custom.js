function getFileControls(val){
	//alert(val);
	var html = '';

	if(val == ''){
		html += "<div class='form-group'><span class='col-md-3 header_form'>Select Image</span><input type='file' name='image' id='image' /></div>"
	}
	
	$("#as_image_controls").html(html);
}

function parseXml(xml) {
   var dom = null;
   if (window.DOMParser) {
      try { 
         dom = (new DOMParser()).parseFromString(xml, "text/xml"); 
      } 
      catch (e) { dom = null; }
   }
   else if (window.ActiveXObject) {
      try {
         dom = new ActiveXObject('Microsoft.XMLDOM');
         dom.async = false;
         if (!dom.loadXML(xml)) // parse error ..

            window.alert(dom.parseError.reason + dom.parseError.srcText);
      } 
      catch (e) { dom = null; }
   }
   else
      alert("cannot parse xml string!");
   return dom;
}


$(document).ready(function(){

	
	/*var method = 'post';
	var url = 'http://www.google.com/trends/hottrends/atom/hourly';
	var xhr = new XMLHttpRequest({mozSystem: true});                    
  	if ("withCredentials" in xhr) {
    	// XHR for Chrome/Firefox/Opera/Safari.
	    xhr.open(method, url, true);
  	} else if (typeof XDomainRequest != "undefined") {
	    // XDomainRequest for IE.
	    xhr = new XDomainRequest();
	    xhr.open(method, url);
  	} else {
	    // CORS not supported.
	    xhr = null;
  	}
  	xhr.onload = function () {
	    console.log(xhr.responseText);
	};
	xhr.send();*/

	//get google trends in dashboard starts
	//$("#google_trends").load('http://www.google.com/trends/hottrends/atom/hourly');
	/*$.ajax({
		url:'http://www.google.com/trends/hottrends/atom/hourly',
		headers:{'Access-Control-Allow-Origin': '*' },
		crossDomain:true,
		type: 'POST',
		success: function(data){
			$("google_trends").html(data);
		}
	});

	var contentType ="application/x-www-form-urlencoded; charset=utf-8";
	 
	if(window.XDomainRequest) //for IE8,IE9
	    contentType = "text/plain";
	 
	$.ajax({
     	url:"http://www.google.com/trends/hottrends/atom/hourly",
     	data:"",
     	type:"POST",
     	dataType:"json",   
     	contentType:contentType, 
     	xhrFields: {
  			withCredentials: true
  		},
  		headers:{
  			'Access-Control-Allow-Origin': '*'
  		},
     	success:function(data){
    		alert("Data from Google"+JSON.stringify(data));
     	},
     	error:function(jqXHR,textStatus,errorThrown){
	        alert("You can not send Cross Domain AJAX requests: "+errorThrown);
     	}
    });*/

	//get google trends in dashboard ends

	$("#btn_login").click(function(){
		var unm = $("#_username").val();
		var pwd = $("#_password").val();

		if(unm == '' || unm == null){
			alert('Please Enter Username.');
			return false;
		}else if(pwd == '' || pwd == null){
			alert('Please Enter Password.');
			return false;
		}else{
			form = document.getElementById('frm_login');
			formdata = new FormData(form);
			$.ajax({
				url : '/login',
				type : 'POST',
				data : formdata,
				contentType : false,
				cache : false,
				processData : false,
				success : function(data){
					if(data == '1'){
						$("#login_suc_msg").show();
						$("#login_suc_msg").html('You are Successfully Logged In.<br/><br/>');
						location.href = '/dashboard';
					}else if(data == '2'){
						$("#login_fail_msg").show();
						$("#login_fail_msg").html('Please Enter User Name and Password!<br/><br/>');
						$("#frm_login").trigger('reset');
					}else if(data == '3'){
						$("#login_fail_msg").show();
						$("#login_fail_msg").html('Please Enter Correct User Name and Password!<br/><br/>');
						$("#frm_login").trigger('reset');
					}else{
						$("#login_fail_msg").show();
						$("#login_fail_msg").html('Something Went Wrong Please Try Again!<br/><br/>');
						$("#frm_login").trigger('reset');
					}
				}
			});
		}
	});

	$('#frm_login').keydown(function(e) {
		var key = e.which;
		if (key == 13) {
			var unm = $("#_username").val();
			var pwd = $("#_password").val();

			if(unm == '' || unm == null){
				alert('Please Enter Username.');
				return false;
			}else if(pwd == '' || pwd == null){
				alert('Please Enter Password.');
				return false;
			}else{
				form = document.getElementById('frm_login');
				formdata = new FormData(form);
				$.ajax({
					url : '/login',
					type : 'POST',
					data : formdata,
					contentType : false,
					cache : false,
					processData : false,
					success : function(data){
						if(data == '1'){
							$("#login_suc_msg").html('You are Successfully Logged In.<br/><br/>');
							location.href = '/dashboard';
						}else if(data == '2'){
							$("#login_fail_msg").html('Please Enter User Name and Password!<br/><br/>');
							$("#frm_login").trigger('reset');
						}else if(data == '3'){
							$("#login_fail_msg").html('Please Enter Correct User Name and Password!<br/><br/>');
							$("#frm_login").trigger('reset');
						}else{
							$("#login_fail_msg").html('Something Went Wrong Please Try Again!<br/><br/>');
							$("#frm_login").trigger('reset');
						}
					}
				});
			}
		}
	});

	$(".list-group-item").click(function(){
		$('.selval').each(function(){
			$(this).prop('checked',false);
		});
		$(this).parent().find('input:radio').prop('checked', true);
		//alert($(this).parent().find('input:radio').val());
	});

	//in add bucket images load image controls on page load
	/*var html = '';
	for (var i = 1; i <= 10; i++) {
		html += "<div class='form-group'><span class='col-md-3 header_form'>Image "+i+"</span><input type='file' name='image"+i+"' /></div>"
	};
	$("#as_image_controls").html(html);*/

	//show add bucket images form in bucket list page
	$("#btn_sho_add_form").click(function(){
		$("#add_form").slideToggle();
	});

	$(".image_picker_image").hover(function(){
		var src = $(this).attr('src');
		var image = '<img src="'+src+'" />';
		$(".image_picker_image").popover({placement: 'bottom', content: image, html: true});
	});

	//add bucket on button click
	$("#btn_add_bucket").click(function(){
		
		$("#btn_add_bucket").hide();
		$("#btn_add_bucket_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){
					form = document.getElementById("frm_add_bucket");
					formdata = new FormData(form);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);

					$.ajax({
						url : '/bucket_name',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							data = data.split("&");
							var status = data[0];
							var message = data[1];

							if(status == '0'){
								alert(message);
							}else if(status == '1'){
								alert(message);
								location.href = '/add_bucket';
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}
							$("#btn_add_bucket").show();
							$("#btn_add_bucket_loading").hide();
						}
					});
				}else{
					location.href = '/';
				}
			}
		});
		
	});
	
	//add bucket on enter key press
	$('#frm_add_bucket').keydown(function(e) {
		var key = e.which;
		if (key == 13) {

			$("#btn_add_bucket").hide();
			$("#btn_add_bucket_loading").show();

			$.ajax({
				url : '/get_session',
				type : 'POST',
				success : function(data){
					if(data != '0'){
						form = document.getElementById("frm_add_bucket");
						formdata = new FormData(form);
						formdata.append('_username',data['_username']);
						formdata.append('_user_id',data['_user_id']);

						$.ajax({
							url : '/bucket_name',
							data : formdata,
							type : 'POST',
							contentType : false,
							cache : false,
							processData : false,
							success : function(data){
								data = data.split("&");
								var status = data[0];
								var message = data[1];

								if(status == '0'){
									alert(message);
								}else if(status == '1'){
									alert(message);
									location.href = '/add_bucket';
								}else if(status == '2'){
									alert(message);
									location.href = '/';
								}
								$("#btn_add_bucket").show();
								$("#btn_add_bucket_loading").hide();
							}
						});
					}else{
						location.href = '/';
					}
				}
			});
		}
	});


	//add bucket images in add bucket
	$("#btn_add_bucket_imges").click(function(){
		
		$("#btn_add_bucket_imges").hide();
		$("#btn_add_bucket_imges_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){
					var id = $("#id").val();
			        /*var search_imgs = [];

			        $.each($("#select_imgs option:selected"), function(){
			            search_imgs.push($(this).val());
			        });*/
					var search_imgs = $("#select_imgs").val();
					
					form = document.getElementById("frm_image");
					formdata = new FormData(form);
					formdata.append('select_imgs',search_imgs);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);

					$.ajax({
						url : '/add_image',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							data = data.split("&");
							var status = data[0];
							var message = data[1];

							if(status == '0'){
								alert(message);
							}else if(status == '1'){
								alert(message);
								location.href = '/add_bucket';
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}
							$("#btn_add_bucket_imges").show();
							$("#btn_add_bucket_imges_loading").hide();
						}
					});
				}else{
					location.href = '/';
				}
			}
		});
		
	});

	//add bucket on enter key press
	$('#frm_image').keydown(function(e) {
		var key = e.which;
		if (key == 13) {
			$("#btn_add_bucket_imges").hide();
			$("#btn_add_bucket_imges_loading").show();

			$.ajax({
				url : '/get_session',
				type : 'POST',
				success : function(data){
					if(data != '0'){
						var id = $("#id").val();
				        /*var search_imgs = [];

				        $.each($("#select_imgs option:selected"), function(){
				            search_imgs.push($(this).val());
				        });*/
						var search_imgs = $("#select_imgs").val();
						
						form = document.getElementById("frm_image");
						formdata = new FormData(form);
						formdata.append('select_imgs',search_imgs);
						formdata.append('_username',data['_username']);
						formdata.append('_user_id',data['_user_id']);

						$.ajax({
							url : '/add_image',
							data : formdata,
							type : 'POST',
							contentType : false,
							cache : false,
							processData : false,
							success : function(data){
								data = data.split("&");
								var status = data[0];
								var message = data[1];

								if(status == '0'){
									alert(message);
								}else if(status == '1'){
									alert(message);
									location.href = '/add_bucket';
								}else if(status == '2'){
									alert(message);
									location.href = '/';
								}
								$("#btn_add_bucket_imges").show();
								$("#btn_add_bucket_imges_loading").hide();
							}
						});
					}else{
						location.href = '/';
					}
				}
			});
		}
	});
	

	//add bucket images from bucket list page on button click
	$("#btn_add_imges_flist").click(function(){
		
		$("#btn_add_imges_flist").hide();
		$("#btn_add_imges_flist_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){

					var id = $("#id").val();
			        /*var search_imgs = [];

			        $.each($("#select_imgs option:selected"), function(){
			            search_imgs.push($(this).val());
			        });*/
					var search_imgs = $("#select_imgs").val();
					
					form = document.getElementById("frm_image_bktlst");
					formdata = new FormData(form);
					formdata.append('select_imgs',search_imgs);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);

					$.ajax({
						url : '/add_image',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							data = data.split("&");
							var status = data[0];
							var message = data[1];

							if(status == '0'){
								alert(message);
							}else if(status == '1'){
								alert(message);
								$("#frm_get_imgs").submit();
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}
							$("#btn_add_imges_flist").show();
							$("#btn_add_imges_flist_loading").hide();
						}
					});
				}else{
					location.href = '/';
				}
			}
		});

	});


	//add bucket images from bucket list page on enter key press
	$('#frm_image_bktlst').keydown(function(e) {
		var key = e.which;
		if (key == 13) {
			$("#btn_add_imges_flist").hide();
			$("#btn_add_imges_flist_loading").show();

			$.ajax({
				url : '/get_session',
				type : 'POST',
				success : function(data){
					if(data != '0'){

						var id = $("#id").val();
				        /*var search_imgs = [];

				        $.each($("#select_imgs option:selected"), function(){
				            search_imgs.push($(this).val());
				        });*/
						var search_imgs = $("#select_imgs").val();
						
						form = document.getElementById("frm_image_bktlst");
						formdata = new FormData(form);
						formdata.append('select_imgs',search_imgs);
						formdata.append('_username',data['_username']);
						formdata.append('_user_id',data['_user_id']);

						$.ajax({
							url : '/add_image',
							data : formdata,
							type : 'POST',
							contentType : false,
							cache : false,
							processData : false,
							success : function(data){
								data = data.split("&");
								var status = data[0];
								var message = data[1];

								if(status == '0'){
									alert(message);
								}else if(status == '1'){
									alert(message);
									$("#frm_get_imgs").submit();
								}else if(status == '2'){
									alert(message);
									location.href = '/';
								}
								$("#btn_add_imges_flist").show();
								$("#btn_add_imges_flist_loading").hide();
							}
						});
					}else{
						location.href = '/';
					}
				}
			});
		}
	});
	

	//update bucket images in update bucket
	$("#btn_update_bucket").click(function(){
		
		$("#btn_update_bucket").hide();
		$("#btn_update_bucket_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){
					form = document.getElementById("frm_update_bucket");
					formdata = new FormData(form);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);

					$.ajax({
						url : '/update_bucket',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							//alert(data);
							data = data.split("&");
							var status = data[0];
							var message = data[1];

							if(status == '0'){
								alert(message);
							}else if(status == '1'){
								alert(message);
								location.href = '/bucket_list';
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}
							$("#btn_update_bucket").show();
							$("#btn_update_bucket_loading").hide();
						}
					});
				}else{
					location.href = '/';
				}
			}
		});
		
	});

	$("#btn_bkt_dlt").click(function(){

		$("#btn_bkt_dlt").hide();
		$("#btn_bkt_dlt_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){
					form = document.getElementById('frm_bkt_dlt');
					formdata = new FormData(form);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);

					$.ajax({
						url : '/delete_bucket',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							data = data.split("&");
							var status = data[0];
							var message = data[1];

							if(status == '1'){
								alert(message);
								location.href = 'bucket_list';
							}else if(status == '3'){
								alert(message);
								location.href = 'bucket_list';
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}else{
								alert(message);
								location.href = '/';
							}
							$("#btn_bkt_dlt").show();
							$("#btn_bkt_dlt_loading").hide();
						}
					});
				}else{
					location.href = '/';
				}
			}
		});
	});

	//search images in add bucket images page after bucket added.
	$("#btn_search").click(function(){
	
		$("#btn_search").hide();
		$("#btn_search_loading").show();

		$.ajax({
			url : '/get_session',
			type : 'POST',
			success : function(data){
				if(data != '0'){
					form = document.getElementById("frm_search");
					formdata = new FormData(form);
					formdata.append('_username',data['_username']);
					formdata.append('_user_id',data['_user_id']);
					
					$.ajax({
						url : '/search_imgs',
						data : formdata,
						type : 'POST',
						contentType : false,
						cache : false,
						processData : false,
						success : function(data){
							data = data.split("&");
							var status = data[0];
							var message = data[1];
							
							if(status == '3'){
								alert(message);
								$("#serched_imgs").html('');
								$("#btn_search").show();
								$("#btn_search_loading").hide();
							}else if(status == '2'){
								alert(message);
								location.href = '/';
							}else if(status == '0'){
								alert(message);
								location.href = '/';
							}else if(status == '1'){
								$("#serched_imgs").html(data.toString().substr(2));
								$("#select_imgs").imagepicker({
						          	hide_select : true,
						          	show_label  : false,
						          	limit       : 1,
						          	clicked     : function(){
						          		var val = $("#select_imgs").val();
						          		getFileControls(val);
						          	},
						          	limit_reached : function(){
						          		alert('You can select Only 1 Images.');
						          	}
						        });

								$("#btn_search").show();
								$("#btn_search_loading").hide();
							}
						}
					});
				}else{
					location.href = '/';
				}
			}
		});
		
	});

	$('#frm_search').keydown(function(e) {
		var key = e.which;
		if (key == 13) {
			$("#btn_search").hide();
			$("#btn_search_loading").show();

			$.ajax({
				url : '/get_session',
				type : 'POST',
				success : function(data){
					if(data != '0'){
						form = document.getElementById("frm_search");
						formdata = new FormData(form);
						formdata.append('_username',data['_username']);
						formdata.append('_user_id',data['_user_id']);
						
						$.ajax({
							url : '/search_imgs',
							data : formdata,
							type : 'POST',
							contentType : false,
							cache : false,
							processData : false,
							success : function(data){
								data = data.split("&");
								var status = data[0];
								var message = data[1];
								
								if(status == '3'){
									alert(message);
									$("#serched_imgs").html('');
									$("#btn_search").show();
									$("#btn_search_loading").hide();
								}else if(status == '2'){
									alert(message);
									location.href = '/';
								}else if(status == '0'){
									alert(message);
									location.href = '/';
								}else if(status == '1'){
									$("#serched_imgs").html(data.toString().substr(2));
									$("#select_imgs").imagepicker({
							          	hide_select : true,
							          	show_label  : false,
							          	limit       : 1,
							          	clicked     : function(){
							          		var val = $("#select_imgs").val();
							          		getFileControls(val);
							          	},
							          	limit_reached : function(){
							          		alert('You can select Only 1 Images.');
							          	}
							        });

									$("#btn_search").show();
									$("#btn_search_loading").hide();
								}
							}
						});																																																																																																																
					}else{
						location.href = '																																																																							';
					}
				}
			});
		}
	});																																																																																														

	//to destroy bucket id from session and render form for add new btn_update_bucket_loading

	$("#btn_new_bucket").click(function(){
		$.ajax({
			url : 'new_bucket_form',
			data : '',
			type : 'POST',
			success : function(data){
				location.reload();
			}
		})
	});

	$(function () {
	    $('.navbar-toggle').click(function () {
	        $('.navbar-nav').toggleClass('slide-in');
	        $('.side-body').toggleClass('body-slide-in');
	        $('#search').removeClass('in').addClass('collapse').slideUp(200);

	        /// uncomment code for absolute positioning tweek see top comment in css
	        //$('.absolute-wrapper').toggleClass('slide-in');
	        
	    });
	   
	   // Remove menu for searching
	   $('#search-trigger').click(function () {
	        $('.navbar-nav').removeClass('slide-in');
	        $('.side-body').removeClass('body-slide-in');

	        /// uncomment code for absolute positioning tweek see top comment in css
	        //$('.absolute-wrapper').removeClass('slide-in');

	    });
	});

	var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});