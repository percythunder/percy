import unittest
from Bucket import app
from StringIO import StringIO

class TestBucketName(unittest.TestCase):
    def test_home(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From /
        response = self.test_app.get('/')

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_login(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.post('/login', data={'_username':'test_bucket','_password':'test_bucket'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_dashboard(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From /
        response = self.test_app.get('/dashboard')

        # Assert response is 302 FOUND.                                           
        self.assertEquals(response.status, "302 FOUND")

    def test_logout(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From /
        response = self.test_app.get('/logout')

        # Assert response is 302 FOUND.                                           
        self.assertEquals(response.status, "302 FOUND")
    
    def test_add_bucket(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the add_bucket
        response = self.test_app.get('/add_bucket')

        # Assert response is 302 FOUND.                                           
        self.assertEquals(response.status, "302 FOUND")

    def test_bucket_name(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.post('/bucket_name', data={'name':'test_bucket','_user_id':'1','_username':'admin'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")


    def test_add_image(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From add_image
        with open('/home/linux/Pictures/natural-scenery.jpg', 'rb') as img1:
            imgStringIO1 = StringIO(img1.read())
        
        response = self.test_app.post('/add_image',content_type='multipart/form-data', 
                                        data={'id':'38',
                                              'weight':'2',
                                              'select_imgs':'',
                                              'header':'from test file',
                                              'body':'from test file, from test file',
                                              'image': (imgStringIO1, 'img1.jpg'),
                                              '_user_id':'1',
                                              '_username':'admin'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_bucket_list(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.get('/bucket_list')

        # Assert response is 302 FOUND.                                           
        self.assertEquals(response.status, "302 FOUND")

    def test_bucket_update_form(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.get('/bucket_update_form')

        # Assert response is 200 OK.   
        self.assertEquals(response.status, "302 FOUND")                

    def test_update_bucket(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From add_image
        with open('/home/linux/Pictures/natural-scenery.jpg', 'rb') as img1:
            imgStringIO1 = StringIO(img1.read())

        response = self.test_app.post('/update_bucket',content_type='multipart/form-data', 
                                        data={'bucket_id':'38',
                                              'id1':'18',
                                              'weight1':'2',
                                              'header1':'from test file update',
                                              'body1':'from test file update, from test file update',
                                              'image1': (imgStringIO1, 'img2.jpg'),
                                              '_user_id':'1',
                                              '_username':'admin'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_delete_bucket(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.post('/delete_bucket', data={'bid':'38','_user_id':'1','_username':'admin'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_search_imgs(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.post('/search_imgs', data={'search':'nature','_user_id':'1','_username':'admin'})

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    def test_new_bucket_form(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        # Make a test request to the bucket app, supplying a fake From bucket_name
        response = self.test_app.post('/new_bucket_form', data={})

        # Assert response is 302 FOUND.                                           
        self.assertEquals(response.status, "200 OK")
