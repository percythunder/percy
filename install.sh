#!/bin/bash

#Write your pre-install script here, for eg:
# - python libraries
# - Any additional software you need to run your code.
# Like:
# pip install flask, etc.

# install all these modules in your system
# pip install requests, pip install flask, pip install pymysql,
# pip install flickr_api, pip install Flask-Redis
# install mysql in your system
# install redis in your system
# create mysql database : bucket
# create tables : 
#   1. table name:bucket {id [primary key, auto increment], name [varchar], datetime} 
#   2. table name:images {id [primary key, auto increment], filename [varchar], bucket_id [int],  
#      datetime[datetime], weight[varchar(50)], header[varchar(50), body[varchar(50), image[varchar(50) } 
#   3. table name:admin_login {admin_id [primary key, auto increment], admin_name [varchar (50)], admin_password [varchar (20)]} 
# for image upload you have to give path where to upload in Bucket.py file to UPLOAD_FOLDER variable
# like this : UPLOAD_FOLDER = 'D:\python project\percy\images'

# To Start Server Run "Bucket.py" file
# To Run "UnitTest python -m unittest -v  BucketTest"

sudo pip install requests
sudo pip install flask
sudo pip install pymysql
sudo pip install flickr_api
sudo pip install Flask-Redis
sudo pip install tweepy
sudo pip install urllib
sudo pip install urllib2
sudo pip install lxml
sudo pip install BeautifulSoup
sudo pip install xmltodict
